package pl.batek.restobrag.data;

import java.util.List;

import pl.batek.restobrag.model.Restaurant;

public interface RestaurantRepository {
	Restaurant addRestaurant(Restaurant restaurant);
	List<Restaurant> getAllRestaurants();
}
