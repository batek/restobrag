package pl.batek.restobrag.web;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.batek.restobrag.model.Restaurant;
import pl.batek.restobrag.service.RestaurantService;

@PropertySource("classpath:config.properties")
@RestController
@Validated
@RequestMapping(value="/restaurants",
		 consumes = MediaType.APPLICATION_JSON_VALUE,
         produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {
	@Autowired
	private Environment env;
	
	private final String NOT_PROVIDED ="";
		
	private RestaurantService service;
	
	@Autowired
	public MainController(RestaurantService service) {
		this.service = service;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Restaurant addRestaurant(@RequestBody @Valid Restaurant restaurant) {
		return this.service.addRestaurant(restaurant);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Restaurant> getRestaurants( 
						@DecimalMin(Restaurant.MIN_LATITUDE) @DecimalMax(Restaurant.MAX_LATITUDE) @RequestParam("latitude")  
						double latitude, 
						@DecimalMin(Restaurant.MIN_LONGITUDE) @DecimalMax(Restaurant.MAX_LONGITUDE) @RequestParam("longitude")  
						double longitude, 
						@RequestParam(value="foodType", defaultValue=NOT_PROVIDED) 
						@Size(max=Restaurant.MAX_FOOD_TYPE_LENGTH) 
						String foodType) {
		
		if(NOT_PROVIDED.equals(foodType)) {
			return this.service.getRestaurantsSortedByDistance(latitude, longitude);
		}
		else {
			return this.service.getRestaurantsSortedByDistanceAndType(latitude, longitude, foodType);
		}
	}

}
