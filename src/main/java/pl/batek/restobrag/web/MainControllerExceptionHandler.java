package pl.batek.restobrag.web;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.batek.restobrag.model.ErrorInfo;

@ControllerAdvice
public class MainControllerExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(MainControllerExceptionHandler.class);
	//TODO Add better exception handling;
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String handleValidationErrors(ConstraintViolationException e) {
		Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
	    StringBuilder strBuilder = new StringBuilder();
	    for(ConstraintViolation<?> v : violations ) {
	    	strBuilder.append(v.getMessage() + "\n");
	    }
	    logger.error(strBuilder.toString());
	    return strBuilder.toString();
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(Exception.class)
	@ResponseBody ErrorInfo handleBadRequest(HttpServletRequest req, Exception ex) {
	    return new ErrorInfo(req.getRequestURL().toString(), ex);
	} 
}
