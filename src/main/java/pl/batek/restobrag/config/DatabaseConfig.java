package pl.batek.restobrag.config;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@PropertySource("classpath:config.properties")
@Configuration
public class DatabaseConfig {
	@Autowired
	private Environment env;

	@Bean
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {

		LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
		sfb.setDataSource(dataSource);
		sfb.setPackagesToScan(env.getProperty("hibernate.packagesToScan"));
		Properties props = new Properties();
		props.setProperty("dialect", env.getProperty("hibernate.dialect"));
		props.setProperty("show_sql", env.getProperty("hibernate.show_sql"));
		props.setProperty("hbm2dll.auto", env.getProperty("hibernate.hbm2dll.auto"));
		sfb.setHibernateProperties(props);
		return sfb;
	}
	
	
	@Bean
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder
			.setType(EmbeddedDatabaseType.HSQL)
			.addScript(env.getProperty("db.create-script"))
			.addScript(env.getProperty("db.populate-script"))
			.build();
		return db;
	}
}
