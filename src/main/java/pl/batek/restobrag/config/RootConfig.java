package pl.batek.restobrag.config;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import pl.batek.restobrag.algorithms.DefaultRestaurantSorter;
import pl.batek.restobrag.algorithms.RestaurantSorter;

@Configuration
@ComponentScan(basePackages = { "pl.batek.restobrag.data", "pl.batek.restobrag.service" })
@Import(DatabaseConfig.class)
public class RootConfig {

	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
	    MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
	    methodValidationPostProcessor.setValidator(validator());

	    return methodValidationPostProcessor;
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
	    return new LocalValidatorFactoryBean();
	}
	
	//	PersistenceExceptionTranslationPostProcessor is a bean post-processor that adds
	//	an adviser to any bean that’s annotated with @Repository so that any platform-specific
	//	exceptions are caught and then rethrown as one of Spring’s unchecked data-access
	//	exceptions.
	@Bean
	public BeanPostProcessor persistenceTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public RestaurantSorter restaurantSorter() {
		return new DefaultRestaurantSorter();
	}
	
	 @Bean  
    public ResourceBundleMessageSource messageSource() {  
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();  
        source.setBasename("messages");  
        source.setUseCodeAsDefaultMessage(true);  
        return source;  
    }  
}
