package pl.batek.restobrag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Restaurant {	
	public final static String MIN_LATITUDE = "-90.00";
	public final static String MAX_LATITUDE = "90.00";
	public final static String MIN_LONGITUDE = "-180.00";
	public final static String MAX_LONGITUDE = "180.00";
	public final static int MAX_FOOD_TYPE_LENGTH = 100;
	public final static int MIN_FOOD_TYPE_LENGTH = 1;
	

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="foodType")
	@Size(min=MIN_FOOD_TYPE_LENGTH, max=MAX_FOOD_TYPE_LENGTH)
	private String foodType;
	
	@Column(name="latitude")
	@DecimalMin(MIN_LATITUDE) 
	@DecimalMax(MAX_LATITUDE) 
	private double latitude;
	
	@Column(name="longitude")
	@DecimalMin(MIN_LONGITUDE) 
	@DecimalMax(MAX_LONGITUDE)
	private double longitude;

	public Restaurant() {
	}
	
	public Restaurant(long id, String name, String foodType, double latitude, double longitude) {
		this.id = id;
		this.name = name;
		this.foodType = foodType;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFoodType() {
		return foodType;
	}
	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}	
	
	@Override
	public boolean equals(Object that) {
		return EqualsBuilder.reflectionEquals(this, that);
	}
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
