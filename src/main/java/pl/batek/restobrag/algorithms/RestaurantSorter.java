package pl.batek.restobrag.algorithms;

import java.util.List;

import pl.batek.restobrag.model.Restaurant;

public interface RestaurantSorter {
	List<Restaurant> sortByDistance(
			List<Restaurant> restaurants, double latitude, double longitude);

	List<Restaurant> sortByDistanceAndType(
			List<Restaurant> restaurants, String foodType, double latitude, double longitude);	
}
