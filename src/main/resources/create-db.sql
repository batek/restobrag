CREATE TABLE restaurant (
  id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  name varchar(256) NOT NULL,
  foodType varchar(100) NOT NULL,
  latitude double NOT NULL,
  longitude double NOT NULL
);
