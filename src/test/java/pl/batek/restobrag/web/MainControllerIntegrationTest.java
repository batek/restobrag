package pl.batek.restobrag.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import pl.batek.restobrag.config.DatabaseConfig;
import pl.batek.restobrag.config.RootConfig;
import pl.batek.restobrag.config.WebConfig;
import pl.batek.restobrag.model.Restaurant;
import pl.batek.restobrag.service.RestaurantService;
import pl.batek.restobrag.test.config.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class, RootConfig.class, DatabaseConfig.class})
@WebAppConfiguration
public class MainControllerIntegrationTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

	private double sampleLatitude = 52.402771;
	private double sampleLongitude = 16.932571;
	private String sampleFoodType = "Fusion";
	private double incorrectLatitude = -200;
	private List<Restaurant> sampleRestaurantsSortedByDistance;
	private List<Restaurant> sampleRestaurantsSortedByDistanceAndType;
	private RestaurantService mockService;
	private String getRestaurantsSortedByDistanceURL;
	private String getRestaurantsSortedByDistanceAndTypeURL;
	private String getRestaurantsWithUncorrectLatitudeURL;
	Restaurant pyraBar;
	Restaurant papierowka;
	Restaurant drukarnia;
	Restaurant mollini;
	Restaurant mykonos;
	
	
	@Before
	public void setUp() {
		
		sampleRestaurantsSortedByDistance = new ArrayList<Restaurant>();
		sampleRestaurantsSortedByDistanceAndType = new ArrayList<Restaurant>();
		getRestaurantsSortedByDistanceURL = "/restaurants"
				+ "?latitude=" + Double.toString(sampleLatitude)
				+ "&longitude=" + Double.toString(sampleLongitude);
		getRestaurantsSortedByDistanceAndTypeURL = getRestaurantsSortedByDistanceURL 
				+ "&foodType=" + sampleFoodType;
		getRestaurantsWithUncorrectLatitudeURL = "/restaurants"
				+ "?latitude=" + Double.toString(incorrectLatitude)
				+ "&longitude=" + Double.toString(sampleLongitude);
		
		
		pyraBar = new Restaurant(1, "Pyra Bar", "Local", 52.404363, 16.933361 );  //187,24m
		papierowka = new Restaurant(2, "Papierowka", "Fusion", 52.404105, 16.934853); //216,04m
		drukarnia = new Restaurant(3, "Drukarnia Sklad Wina i Chleba", "Fusion", 52.406507, 16.929359); //468,14m
		mollini = new Restaurant(4, "Mollini", "Italian", 52.406320, 16.925953); //609,22m
		mykonos = new Restaurant(5, "Mykonos", "Greek", 52.408558, 16.925779); //796,14m
		
		
		
		sampleRestaurantsSortedByDistance.add(pyraBar);
		sampleRestaurantsSortedByDistance.add(papierowka);
		sampleRestaurantsSortedByDistance.add(drukarnia);
		sampleRestaurantsSortedByDistance.add(mollini);
		sampleRestaurantsSortedByDistance.add(mykonos);
		
		sampleRestaurantsSortedByDistanceAndType.add(papierowka);
		sampleRestaurantsSortedByDistanceAndType.add(drukarnia);
		sampleRestaurantsSortedByDistanceAndType.add(pyraBar);
		sampleRestaurantsSortedByDistanceAndType.add(mollini);
		sampleRestaurantsSortedByDistanceAndType.add(mykonos);
	}
	

	
	@Test
	public void shouldGetRestaurantsSortedByDistance() throws Exception {
		MockMvc mockMvc = webAppContextSetup(webApplicationContext).build();
		mockMvc.perform( get(getRestaurantsSortedByDistanceURL).contentType(MediaType.APPLICATION_JSON) )
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", hasSize(5)))
        .andExpect(jsonPath("$[0].name", is(pyraBar.getName())))
        .andExpect(jsonPath("$[0].foodType", is(pyraBar.getFoodType())))
        .andExpect(jsonPath("$[0].latitude", is(pyraBar.getLatitude())))
        .andExpect(jsonPath("$[0].longitude", is(pyraBar.getLongitude())))
        .andExpect(jsonPath("$[1].name", is(papierowka.getName())))
        .andExpect(jsonPath("$[2].name", is(drukarnia.getName())))
        .andExpect(jsonPath("$[3].name", is(mollini.getName())))
        .andExpect(jsonPath("$[4].name", is(mykonos.getName())));
			 
	}
}
